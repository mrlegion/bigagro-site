var pkg = require('../package.json');

// Основные пути
var _src    = './app/',     // Папка с исходными файлами
    _public = './public/',  // Папка с выведенным проектом
    _tmp    = './tmp/';     // Папка временых файлов

var _css    = 'css/',
    _html   = 'html/',
    _js     = 'js/',
    _assets = 'assets/',
    _img    = 'img/',
    _data   = 'data/',
    _svg    = 'svg/',
    _font   = 'fonts/';

module.exports = {
    path: {
        _src    : './app/',
        _public : './public/',
        _tmp    : './tmp/',
        _css    : 'css/',
        _html   : 'html/',
        _js     : 'js/',
        _assets : 'assets/',
        _font   : 'fonts/',
        _img    : 'img/',
        _data   : 'data/',
        _svg   : 'svg/',
        _bower  : './bower_components/'
},
    // Очистка временных дерикторий
    clean: {
        _public: _public,
        _tmp: _tmp
    },
    // Настройки для сервера
    browser: {
        server : {
            baseDir: './public'
        },
        notify : false
    },
    // Инициализация параметров для CSS
    css: {
        src: _src + _css,
        dest: _public + _css,
        params : {
            'include css': true
        },
        autoprefixer: {
            browsers: ['> 1%', 'last 2 versions'],
            cascade: false
        },
        compress : false,
        compressParams: {},
        sourcemap : true
    },
    // Инициализация параметров для HTML
    html: {
        src: _src + _html,
        dest: _public,
        useJson: true,
        params: {
            pretty : true,
            locals : {
                pkgVersion: pkg.version
            }
        }
    },
    // Инициализация параметров для JS
    js: {
        src: _src + _js,
        dest: _public + _js,
        name: 'main.js',
        concat: false
    },
    // Инициализация параметров для Assets
    assets : {
        src: _src + _assets,
        js: _public + _js,
        css: _public + _css,
        font: {
            src: _src + _assets + _font,
            dest: _public + _font
        },
    },
    // Инициализация параметров для Marge Json
    margeJson: {
        yaml: {
            filename: 'data-yaml.json'
        },
        json: {
            filename: 'data-json.json'
        },
        build: {
            filename: 'data.json'
        }
    },
    // Инициализация параметров для изображений
    image: {
        src: _src + _img,
        dest: _public + _img,
        svg: {
            source : _src + _img + _svg,
            dest : _public + _img + _svg
        }
    }
};