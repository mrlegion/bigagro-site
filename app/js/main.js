$(document).ready(function() {
    /* -------------------------------
    Catalog vertical navigation menu
    ------------------------------- */
    $(function () {
        var catalog_category_link = $(".navigation-vertical-category a");
        var catalog_menu_block = $(".navigation-vertical-menu .navigation-vertical-menu__block");
        var prev_id;
        catalog_menu_block.first().addClass("js_menu_visible_yes");

        catalog_category_link.mouseover(function () {

            var id = $(this).attr("data-id");
            if (id != prev_id) {
                catalog_category_link.removeClass("js_hover_yes");
                $(this).removeClass("js_hover_no");
                $(this).addClass("js_hover_yes");
            }

            catalog_menu_block.each(function () {
                var id_elem = $(this).attr("data-id");

                $(this).removeClass("js_menu_visible_yes");
                $(this).addClass("js_menu_visible_no");

                if (id_elem == id)
                {
                    $(this).addClass("js_menu_visible_yes");
                }
            });
        });
        
        catalog_category_link.mouseout(function () {
            prev_id = $(this).attr("data-id");
        });

        $("body").on('click', function(e){
            var menu = $(".dropdown");
            if (!menu.is(e.target) && menu.has(e.target).length === 0){
                menu.removeClass("show");
                $(".nav-catalog__toggle").removeClass("nav-catalog__active");
            }
        });
    });

    /* -------------------------------
    Button catalog nav activate
    ------------------------------- */
    $(function(){
        var link = $(".nav-catalog__toggle");
        link.click(function(e){
            if (link.hasClass("nav-catalog__active")) {
                link.removeClass("nav-catalog__active");
                return;
            }

            link.addClass("nav-catalog__active");
        });
    });

    /* -------------------------------
    Tabs controll
    ------------------------------- */
    $(function(){
        $(".tabs_block").on("click", ".tabs_block_item", function(){
            $(".tabs_block").find(".tabs_block_item-active").removeClass("tabs_block_item-active");
            $(".tabs_block").find(".tabs_block_wrapper-active").removeClass("tabs_block_wrapper-active");
            $(this).addClass("tabs_block_item-active");

            $(".tabs_block_wrapper").eq($(this).index()).addClass("tabs_block_wrapper-active").fadeIn(200);
        });
    });

    /* -------------------------------
    uploads files
    ------------------------------- */
    'use strict';

;( function( $, window, document, undefined )
{
	$( '.input_file' ).each( function()
	{
		var $input	 = $( this ),
            $label	 = $input.next('label').find(".input_file_label_text"),
			labelVal = $label.html();

        $input.on( 'change', function( e )
		{
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName ) {
                $label.html( fileName );
                console.log(1);
            }
			else {
                $label.html( labelVal );
                console.log(2);
            }

            console.log(fileName);
		});

		// Firefox bug fix
		$input
		.on( 'focus', function(){ $input.addClass( 'has-focus' ); })
		.on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
	});
})( jQuery, window, document );
});

