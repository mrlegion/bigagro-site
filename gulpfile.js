var gulp            = require('gulp'),
    config          = require('./gulp/config'),
    finder          = require('./gulp/finder'),
    del             = require('del'),
    browser         = require('browser-sync'),
    prefixer        = require('gulp-autoprefixer'),
    concat          = require('gulp-concat'),
    filter          = require('gulp-filter'),
    flatten         = require('gulp-flatten'),
    gulpIf          = require('gulp-if'),
    jade            = require('gulp-jade'),
    merge           = require('gulp-merge-json'),
    minify          = require('gulp-minify-css'),
    rename          = require('gulp-rename'),
    sourcemap       = require('gulp-sourcemaps'),
    stylus          = require('gulp-stylus'),
    yaml            = require('gulp-yaml'),
    run             = require('run-sequence'),
    fs              = require('fs'),
    mainBowerFiles  = require('main-bower-files'),
    CDN             = require('gulp-google-cdn'),
    cache           = require('gulp-cache'),
    imagemin        = require('gulp-imagemin');

// ==================================================
// Серверные базовые команды
// Clean Task
// ==================================================
gulp.task('cleanup' , function() {
    return del([].concat(
        config.clean._public + '**/*',
        config.clean._tmp + '**/*'
    ));
});

// ==================================================
// Browser-Sync Task
// ==================================================
gulp.task('browser-sync' , function() {
    return browser.init(config.browser);
});

// ==================================================
// Bower Task
// ==================================================

gulp.task('bower-js' , function() {
    return gulp.src(mainBowerFiles( '**/*.js' ), {base: './bower_components/'})
        .pipe( gulp.dest(config.path._src + config.path._assets) );
});

gulp.task('bower-css' , function() {
    return gulp.src(mainBowerFiles( '**/*.css' ), {base: './bower_components/'})
        .pipe( gulp.dest(config.path._src + config.path._assets) );
});

gulp.task('bower', function (cb) {
    return run(['bower-js', 'bower-css'], cb);
});

// ==================================================
// Data json and yml task
// ==================================================
gulp.task('yaml' , function() {
    return gulp.src(['**/*.yml' , '!**/_*.yml'] , { cwd : config.html.src })
        .pipe(yaml({space: '\t'}))
        .pipe(merge(config.margeJson.yaml))
        .pipe(gulp.dest( config.path._tmp + config.path._data ));
});

gulp.task('json' , function() {
    return gulp.src(['**/*.json' , '!**/_*.json'] , { cwd : config.html.src })
        .pipe(merge(config.margeJson.json))
        .pipe(gulp.dest( config.path._tmp + config.path._data ));
});

gulp.task('all-data' , function() {
    return gulp.src('**/*.json' , { cwd : config.path._tmp + config.path._data })
        .pipe(merge(config.margeJson.build))
        .pipe(rename('data.json'))
        .pipe(gulp.dest( config.path._tmp ));
});

// ==================================================
// Jade task
// ==================================================
gulp.task('jade' , function() {
    config.html.params.locals = (config.html.useJson ? JSON.parse(fs.readFileSync( config.path._tmp + 'data.json' )) : '');

    return gulp.src(['*.jade' , '!_*.jade'] , { cwd : config.html.src })
        .pipe(filter(function (file) {
            return !/app[\\\/]jade[\\\/]components/.test(file.path);
        }))
        .pipe(jade( config.html.params ))
        .pipe(flatten())
        .pipe(gulp.dest( config.html.dest ))
        .pipe(browser.reload({stream : true}));
});

// ==================================================
// Stylus task
// ==================================================
gulp.task('stylus' , function() {
    return gulp.src(['*.styl' , '!_*.styl'] , { cwd : config.css.src })
        .pipe(gulpIf(config.css.sourcemap, sourcemap.init()))
        .pipe(stylus( config.css.params ))
        .pipe(prefixer( config.css.autoprefixer ))
        .pipe(gulpIf(config.css.compress, minify(config.css.compressParams)))
        .pipe(gulpIf(config.css.sourcemap, sourcemap.write('maps/')))
        .pipe(gulp.dest( config.css.dest ))
        .pipe(browser.reload({stream : true}));
});

// ==================================================
// Scripts task
// ==================================================
gulp.task('scripts' , function() {
    return gulp.src(['**/*.js' , '!**/_*.js'] , { cwd : config.js.src })
        .pipe(flatten())
        .pipe(gulpIf(config.js.concat, concat( config.js.name )))
        .pipe(gulp.dest( config.js.dest ))
        .pipe(browser.reload({stream: true}));
});

// ==================================================
// Assets task
// ==================================================
gulp.task('assets:font', function () {
    gulp.src('**/*.*', { cwd : config.assets.font.src })
        .pipe(flatten())
        .pipe(gulp.dest(config.assets.font.dest));
});

gulp.task('assets:css', function () {
    var css = filter('**/*.css', { restore: true });

    return gulp.src(['**/*.*', '!**/_*.*'], { cwd : config.assets.src })
        .pipe(css)
        .pipe(flatten())
        .pipe(gulp.dest(config.assets.css))
        .pipe(css.restore)
        .pipe(browser.reload({stream: true}));
});

gulp.task('assets:js', function () {
    var js = filter(['**/*.min.js', '**/*.js'], { restore: true });

    return gulp.src(['**/*.*', '!**/_*.*'], { cwd : config.assets.src })
        .pipe(js)
        .pipe(flatten())
        .pipe(gulp.dest(config.assets.js))
        .pipe(js.restore)
        .pipe(browser.reload({stream: true}));
});

gulp.task('assets', function (cb) {
    return run('assets:font', 'assets:css', 'assets:js', cb );
});

// ==================================================
// Image task
// ==================================================
gulp.task('image', function () {
    var image = filter('**/*.{jpg,jpeg,png,gif}', {restore: true});

    gulp.src(['**/*.*', '!**/_*.*'], {cwd: config.image.src})
        .pipe(image)
        .pipe(cache(imagemin({progressive: true})))
        .pipe(gulp.dest(config.image.dest))
        .pipe(image.restore)
        .pipe(browser.reload({stream: true}));
});

gulp.task('svg', function(){
    gulp.src('**/**/*.svg', {cwd : config.image.svg.source})
        .pipe(gulp.dest(config.image.svg.dest));
});

// ==================================================
// SERVER TASK
// Compile all data json and yaml
// ==================================================
gulp.task('data' , function (cb) {
    return run( [ 'yaml', 'json' ], 'all-data', cb );
});

gulp.task('html' , function (cb) {
    return run( [ 'data' ], 'jade', cb );
});

/*
 // Library transfer task
 gulp.task('assets' , function (cb) {
 return run(
 'images',
 'svg',
 'lib-scripts',
 'lib-styles',
 cb
 );
 });*/

// ==================================================
// MAIN TASK
// Build project task
// ==================================================
gulp.task('build' , function(cb) {
    return run( ['cleanup'], [ 'html' ], 'stylus', ['assets', 'image', 'svg', 'scripts'], cb );
    // return run( ['cleanup', 'bower'], [ 'html' ], 'stylus', cb );
});

// ==================================================
// Develop task (use when codding)
// ==================================================
gulp.task('dev' , function(cb) {
    return run( 'build', [ 'browser-sync', 'watch' ],  cb );
});

// ==================================================
// Watchers task
// Watch
// ==================================================
gulp.task('watch' , function(cb) {

    // Modules data
    gulp.watch([ config.html.src + '**/**/**/**/**/*.{json,yml}'] , ['html']);

    // Modules, pages
    gulp.watch( config.html.src + '**/**/**/**/*.jade' , ['jade']);

    // Statics styles
    gulp.watch( config.css.src + '**/**/**/**/**/*.styl' , ['stylus']);

    // Scripts file
    gulp.watch( config.js.src + '**/*.js' , ['scripts']);

    // Images file
    gulp.watch('./app/img/**/*.{ jpg,jpeg,png,gif }' , ['images']);

    // SVG file
    gulp.watch('./app/img/svg/**/*.svg' , ['svg']);

    // Assets file
    //gulp.watch('./app/assets/**/*' , ['assets']);
});